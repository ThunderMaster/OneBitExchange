# README

Docker Comands

- docker-compose run --rm app bundle install

- docker-compose run --rm app bundle exec rails db:create

- docker-compose run --rm app bash

- docker-compose up

- docker-compose run --rm app bundle exec rspec spec/requests/exchanges_spec.rb

- docker-compose run --rm app bundle exec rspec spec/system/exchanges_index_system_spec.rb
